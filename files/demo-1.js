(function() {
    
    var   b2Vec2 = Box2D.Common.Math.b2Vec2
        , b2AABB = Box2D.Collision.b2AABB
        , b2BodyDef = Box2D.Dynamics.b2BodyDef
        , b2Body = Box2D.Dynamics.b2Body
        , b2FixtureDef = Box2D.Dynamics.b2FixtureDef
        , b2Fixture = Box2D.Dynamics.b2Fixture
        , b2World = Box2D.Dynamics.b2World
        , b2MassData = Box2D.Collision.Shapes.b2MassData
        , b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape
        , b2CircleShape = Box2D.Collision.Shapes.b2CircleShape
        , b2DebugDraw = Box2D.Dynamics.b2DebugDraw
        , b2RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef
        , b2MouseJointDef =  Box2D.Dynamics.Joints.b2MouseJointDef;

    window.requestAnimFrame = (function(){
        return  window.requestAnimationFrame       || 
        window.webkitRequestAnimationFrame || 
        window.mozRequestAnimationFrame    || 
        window.oRequestAnimationFrame      || 
        window.msRequestAnimationFrame     || 
        function(/* function */ callback, /* DOMElement */ element){
            window.setTimeout(callback, 1000 / 60);
        };
    })();

    var SCALE,
        canvas,
        ctx,
        world,
        fixDef,
        shapes = {},
		clouds = {},
		followed = {x:0, y:0};
        
    var debug = false;
    
    var init = {
        start: function(id) {

            this.defaultProperties();
            this.canvas(id);
            
            box2d.create.world();
            box2d.create.defaultFixture();
            
            this.surroundings.leftWall();
            this.surroundings.rightWall();
			for(var i=0; i<Math.ceil((canvas.width*3)/85); i++) {
				this.surroundings.ground(i);
			}
			
            this.surroundings.ceiling();
            this.callbacks();

            (function hell() {
                loop.step();
                loop.update();
                if (debug) {
                    world.DrawDebugData();
                }
                loop.draw();
                requestAnimFrame(hell);
            })();
        },
        defaultProperties: function() {
            SCALE = 30;
        },
        canvas: function(id) {
            canvas = document.getElementById(id);
            ctx = canvas.getContext("2d");
        },

			
		
		
        surroundings: {
		
            rightWall: function() {
                add.box({
				    height: canvas.height/SCALE,   // 380px / 30
                    width:2,
                    x: (canvas.width*3)/SCALE+1.1,        // 740 / 30 + 1.1
                    y: canvas.height/SCALE/2,        // 380px / 30 / 2
                    isStatic: true
                });
            },
            ground: function(i) {
                add.box({
					height: 85/SCALE,
                    width: 85/SCALE,     // 740 / 30
                    x: (i*85+85/2)/SCALE,        // 740 / 30 / 2
                    y: (canvas.height-85+85/2)/SCALE,
					sh: 'ground',
                    isStatic: true
                });
            },
            leftWall: function() {
                add.box({
				    height: canvas.height/SCALE,   // 380px / 30
                    width:2,
                    x: 0-1.1,
                    y: canvas.height/SCALE/2,        // 380px / 30 / 2
                    isStatic: true
                });
            },
			ceiling: function() {
                add.box({
				    height: 2,   
                    width: (canvas.width*3)/SCALE,
                    x: (canvas.width*3)/SCALE/2,     // 740 / 30 / 2
                    y: 0-1.1,        
                    isStatic: true
                });
            }
        },
        callbacks: function() {
            canvas.addEventListener('click', function(e) {
                var shapeOptions = {
					x: 3, 
					y: (canvas.height-85.0-85.0/2)/SCALE,
					impulse: true,
					mouseX: (e.pageX - $(this).offset().left)/3,
					mouseY: -($( window ).height()-e.pageY - ($( window ).height() - ($(this).offset().top+canvas.height)))/3,
					sh: 'circle'
                };
				
				add.circle(shapeOptions);
            }, false);
        }
    };        
     
     
    var add = { 
        circle: function(options) {
            options.radius = 42.5/SCALE; 
            var shape = new Circle(options);
			followed = shape;
            shapes[shape.id] = shape;
            box2d.addToWorld(shape);
        },
        box: function(options) {
            options.width = options.width || 85/SCALE; 
            options.height = options.height || 85/SCALE; 
            var shape = new Box(options);
            shapes[shape.id] = shape;
            box2d.addToWorld(shape);
        }
    };

    var box2d = {
        addToWorld: function(shape) {
            var bodyDef = this.create.bodyDef(shape);
            var body = world.CreateBody(bodyDef);
            if (shape.radius) {
                fixDef.shape = new b2CircleShape(shape.radius);
            } else {
                fixDef.shape = new b2PolygonShape;
                fixDef.shape.SetAsBox(shape.width/2, shape.height/2);
            }
            body.CreateFixture(fixDef);
			
			if(shape.impulse == true) {
				body.ApplyImpulse(
				new b2Vec2(shape.mouseX, shape.mouseY),
				body.GetWorldCenter()
				);
			}
        },
        create: {
            world: function() {
                world = new b2World(
                    new b2Vec2(0, 10)    //gravity
                    , false              //allow sleep
                );
                
                if (debug) {
                    var debugDraw = new b2DebugDraw();
                    debugDraw.SetSprite(ctx);
                    debugDraw.SetDrawScale(30.0);
                    debugDraw.SetFillAlpha(0.3);
                    debugDraw.SetLineThickness(1.0);
                    debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
                    world.SetDebugDraw(debugDraw);
                }
            },
            defaultFixture: function() {
                fixDef = new b2FixtureDef;
                fixDef.density = 1.0;
                fixDef.friction = 0.5;
                fixDef.restitution = 0.2;
            },
            bodyDef: function(shape) {
                var bodyDef = new b2BodyDef;
        
                if (shape.isStatic == true) {
                    bodyDef.type = b2Body.b2_staticBody;
                } else {
                    bodyDef.type = b2Body.b2_dynamicBody;
                }
                bodyDef.position.x = shape.x;
                bodyDef.position.y = shape.y;
                bodyDef.userData = {id: shape.id, img: shape.img};
                bodyDef.angle = shape.angle;
            
                return bodyDef;
            }
        },
        get: {
            bodySpec: function(b) {
                return {
                    x: b.GetPosition().x, 
                    y: b.GetPosition().y, 
                    angle: b.GetAngle(), 
                    center: {
                        x: b.GetWorldCenter().x, 
                        y: b.GetWorldCenter().y
                    }
                };
            }
        }
    };


    var loop = {
        step: function() {
            var stepRate = 1 / 60;
            world.Step(stepRate, 10, 10);
            world.ClearForces();
        },
        update: function () {            
            for (var b = world.GetBodyList(); b; b = b.m_next) {
                if (b.IsActive() && b.GetUserData() !== null && typeof b.GetUserData() !== 'undefined' && b.GetUserData().id !== null && typeof b.GetUserData().id !== 'undefined') {
                    shapes[b.GetUserData().id].update(box2d.get.bodySpec(b));
                }
            }
        },
        draw: function() {            
            if (!debug) { 
					ctx.clearRect(0, 0, canvas.width, canvas.height);
					var my_gradient=ctx.createLinearGradient(0,0,0,canvas.height);
					my_gradient.addColorStop(0,"#1CC1FF");
					my_gradient.addColorStop(0.5,"#69D2FA");
					my_gradient.addColorStop(1,"#98E3FF");
					ctx.fillStyle=my_gradient;
					ctx.fillRect(0,0,canvas.width,canvas.height);
			}
            
					
			var whetherDraw = Math.round(Math.random()*120);
			switch(whetherDraw) {
				case 0: var id = Math.round(Math.random() * 200000); clouds[id] = new Cloud(0,id); break;
				case 1: var id = Math.round(Math.random() * 200000); clouds[id] = new Cloud(1,id); break;
				default: break;
			}
			
					
			for (var i in clouds) {
                clouds[i].update();
            }
			

            for (var i in shapes) {
                shapes[i].draw();
            }
						
        }
    };    
    
    var helpers = {
        randomImg: function(shape) {
			var img = '';
			if(shape == 'circle') {
				img = 'kula';
				var digits = '1.2.3'.split('.');
				img += digits[Math.round(Math.random() * 2)];
				img += '.png';
			} else if(shape == 'box'){
				img = 'klocek';
				var digits = '1.2.3'.split('.');
				img += digits[Math.round(Math.random() * 2)];
				img += '.png';
			} else if(shape == 'ground') {
				img = 'ground.png';
			}		
				return img;
			}
    };
    

    var Shape = function(v) {
	    this.impulse = v.impulse || false;
        this.id = Math.round(Math.random() * 1000000);
        this.x = v.x || Math.random()*23 + 1;
        this.y = v.y || 0;
        this.angle = 0;
		this.sh = v.sh || null;
		this.mouseX = v.mouseX || null;
		this.mouseY = v.mouseY || null;
        this.img = helpers.randomImg(v.sh);
        this.center = { x: null, y: null };
        this.isStatic = v.isStatic || false;
        
        this.update = function(options) {
            this.angle = options.angle;
            this.center = options.center;
            this.x = options.x;
            this.y = options.y;
        };
    };
    
    var Circle = function(options) {
        Shape.call(this, options);
        this.radius = options.radius || 1;
        
        this.draw = function() {
				
					ctx.save();
						
	
					if(followed.y*SCALE <= canvas.height/2) {  
            
						if(this.x<=followed.x && this.y<=followed.y) {
								ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, canvas.height/2 - Math.abs(this.y-followed.y)*SCALE);
						}else if(this.x>followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, canvas.height/2 + Math.abs(this.y-followed.y)*SCALE);
						}else if(this.x<=followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, canvas.height/2 + Math.abs(this.y-followed.y)*SCALE);
						}else if(this.x>followed.x && this.y<=followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, canvas.height/2 - Math.abs(this.y-followed.y)*SCALE);
					  }
					  
					} else {
					
						 if(this.x<=followed.x && this.y<=followed.y) {
								ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
						}else if(this.x>followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
						}else if(this.x<=followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
						}else if(this.x>followed.x && this.y<=followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
					  }
					  
					}
						
						
						
            ctx.rotate(this.angle);

			if(this.img!='') {
				var img = new Image(2*this.radius*SCALE,2*this.radius*SCALE);
				img.src = this.img;
				ctx.drawImage(img,(/*this.x*/-this.radius)*SCALE,(/*this.y*/-this.radius)*SCALE);	
            }			
            ctx.restore();
						
        };
    };
	
	
    Circle.prototype = Shape;
    
    var Box = function(options) {
        Shape.call(this, options);
        this.width = options.width || Math.random()*2+0.5;
        this.height = options.height || Math.random()*2+0.5;
        
        this.draw = function() {
							
				
				ctx.save();		
						
				if(followed.y*SCALE <= canvas.height/2) {  
						
						if(this.x<=followed.x && this.y<=followed.y) {
								ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, canvas.height/2 - Math.abs(this.y-followed.y)*SCALE);
						}else if(this.x>followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, canvas.height/2 + Math.abs(this.y-followed.y)*SCALE);
						}else if(this.x<=followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, canvas.height/2 + Math.abs(this.y-followed.y)*SCALE);
						}else if(this.x>followed.x && this.y<=followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, canvas.height/2 - Math.abs(this.y-followed.y)*SCALE);
						}
						
					} else {
						 if(this.x<=followed.x && this.y<=followed.y) {
								ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
						}else if(this.x>followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
						}else if(this.x<=followed.x && this.y>followed.y) {
						    ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
						}else if(this.x>followed.x && this.y<=followed.y) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x)*SCALE, this.y*SCALE);
					  }
					}
						
            ctx.rotate(this.angle);
            
			if(this.img!='') {
				var img = new Image(this.width*SCALE,this.height*SCALE);
				img.src = this.img;
				ctx.drawImage(img,(/*this.x*/-(this.width/2))*SCALE,(/*this.y*/-(this.height/2))*SCALE); //nie potrz. this.x			
            }
            ctx.restore();
						
        };
    };
    Box.prototype = Shape;
    
	
	var Cloud = function(r,id) {
        
		this.id = id;
		
		if(r == 0) {
		    this.img = 'cloud1.png';
			this.width = 121;
			this.height = 73;
		    
		} else {
		    this.img = 'cloud2.png';
		    this.width = 101;
			this.height = 61;
        }		
		
		this.speedX = Math.ceil(Math.random()*4+0.1)
		this.x = canvas.width*3;
		this.y = Math.round(Math.random()*(canvas.height-140));
	    
		
		this.update = function() {
		    if(this.x + this.width < 0) {
			    for(var i=0; i<clouds.length; i++) {
				     if(clouds[i] == this) { clouds.splice(this.index,1) }
				}
			} else {
			   this.x -= this.speedX;
			   this.draw();
			}
		};
		
        this.draw = function() {
            ctx.save();
            
					
				if(followed.y*SCALE <= canvas.height/2) {  
						
						if(this.x<=followed.x*SCALE && this.y<=followed.y*SCALE) {
								ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x*SCALE), canvas.height/2 - Math.abs(this.y-followed.y*SCALE));
						}else if(this.x>followed.x*SCALE && this.y>followed.y*SCALE) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x*SCALE), canvas.height/2 + Math.abs(this.y-followed.y*SCALE));
						}else if(this.x<=followed.x*SCALE && this.y>followed.y*SCALE) {
						    ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x*SCALE), canvas.height/2 + Math.abs(this.y-followed.y*SCALE));
						}else if(this.x>followed.x*SCALE && this.y<=followed.y*SCALE) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x*SCALE), canvas.height/2 - Math.abs(this.y-followed.y*SCALE));
						}
						
						
				} else {
				
						if(this.x<=followed.x*SCALE && this.y<=followed.y*SCALE) {
								ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x*SCALE), this.y);
						}else if(this.x>followed.x*SCALE && this.y>followed.y*SCALE) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x*SCALE), this.y);
						}else if(this.x<=followed.x*SCALE && this.y>followed.y*SCALE) {
						    ctx.translate(canvas.width/2 - Math.abs(this.x-followed.x*SCALE), this.y);
						}else if(this.x>followed.x*SCALE && this.y<=followed.y*SCALE) {
						    ctx.translate(canvas.width/2 + Math.abs(this.x-followed.x*SCALE), this.y);
					    }
						
				}
								
				var img = new Image(this.width,this.height);
				img.src = this.img;
								
												
				ctx.drawImage(img,this.x,this.y); //nie potrz this.x			
				ctx.restore();
						
        };
    };
	
    init.start('box2d-demo');
})();
